CFLAGS += -lavutil -lavformat -lavcodec -lswscale -lz -lm `sdl-config --cflags --libs`

all:
	gcc -o tutorial01 tutorial01.c $(CFLAGS)
	gcc -o tutorial02 tutorial02.c $(CFLAGS)
	gcc -o tutorial03 tutorial03.c $(CFLAGS)
	gcc -o tutorial04 tutorial04.c $(CFLAGS)
	gcc -o tutorial05 tutorial05.c $(CFLAGS)
	gcc -o tutorial06 tutorial06.c $(CFLAGS)
	gcc -o tutorial07 tutorial07.c $(CFLAGS)

clean:
	rm tutorial01 tutorial02 tutorial03 tutorial04 tutorial05 tutorial06 tutorial07